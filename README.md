# Description
The data is about recent tweets of Pfizer & BioNTech vaccine.

The data is collected using tweepy Python package to access Twitter API.

Study the subjects of recent tweets about the vaccine made in collaboration by Pfizer and BioNTech, perform various NLP tasks on this data source.

# Note
The data was taken from kaggle public dataset.
https://www.kaggle.com/gpreda/pfizer-vaccine-tweets

